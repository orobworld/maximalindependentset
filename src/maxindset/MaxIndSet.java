package maxindset;

import bdd.BDD;
import java.io.IOException;

/**
 * MaxIndSet demonstrates the use of the JGraphT library to build and solve
 * a binary decision diagram for a maximal independent set problem.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class MaxIndSet {
  private static final long SEED = 456;        // random number seed
  private static final int MAXWEIGHT = 100;    // maximum possible vertex weight
  // File paths for graph output.
  private static final String CPATH = "/tmp/chvatal.gv";  // Chvatal graph
  private static final String BPATH = "/tmp/bdd.gv";      // BDD graph

  /**
   * Dummy constructor.
   */
  private MaxIndSet() { }

  /**
   * Runs the demonstration.
   * @param args the command line arguments (ignored)
   */
  public static void main(final String[] args) {
    // Create a problem instance.
    Problem problem = new Problem(SEED, MAXWEIGHT);
    System.out.println(problem);
    try {
      problem.export(CPATH);
    } catch (IOException ex) {
      System.out.println("Failed to export the Chvatal graph: "
                         + ex.getMessage());
    }
    // Create a BDD for the problem.
    BDD bdd = new BDD(problem);
    try {
      bdd.export(BPATH);
    } catch (IOException ex) {
      System.out.println("Failed to export the BDD graph: "
                         + ex.getMessage());
    }
    // Display the total weight of the maximal independent set.
    System.out.println("\nThe maximal independent set has total weight "
                       + (int) bdd.getLongestPathLength());
    // Display the maximal independent set.
    System.out.println("\nThe maximal independent set is:");
    System.out.println(bdd.getMIS());
  }

}
