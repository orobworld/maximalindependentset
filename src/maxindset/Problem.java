package maxindset;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.jgrapht.Graph;
import org.jgrapht.Graphs;
import org.jgrapht.generate.NamedGraphGenerator;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;
import org.jgrapht.nio.dot.DOTExporter;

/**
 * Problem holds an instance of the maximal independent set problem.
 *
 * The problem instance uses the Chvatal graph with randomly assigned vertex
 * weights.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class Problem {
  private final Graph<Integer, DefaultEdge> chvatal;  // the Chvatal graph
  private final int[] weight;                         // the vertex weights
  private final int nVertices;                        // vertex count

  /**
   * Constructor.
   * @param seed the seed for the random number generator.
   * @param maxWeight the maximum weight assignable to a vertex
   */
  public Problem(final long seed, final int maxWeight) {
    // Generate the graph.
    chvatal = NamedGraphGenerator.chvatalGraph();
    nVertices = chvatal.vertexSet().size();  // 12
    // Add random (positive) node weights.
    weight = (new Random(seed)).ints(nVertices, 0, maxWeight).toArray();
  }

  /**
   * Describes the graph.
   * @return a string displaying the node weights
   */
  public String toString() {
    StringBuilder sb = new StringBuilder("Chvatal graph nodes (weights):\n");
    for (int i = 0; i < nVertices; i++) {
      sb.append("\t").append(String.format("%2d", i)).append(" (")
        .append(String.format("%d", weight[i])).append(")\n");
    }
    sb.append("Chvatal graph edges:\n");
    ArrayList<String> elist = new ArrayList<>();
    for (DefaultEdge e : chvatal.edgeSet()) {
      elist.add(e.toString());
    }
    Collections.sort(elist);
    for (String e : elist) {
      sb.append("\t").append(e).append("\n");
    }
    return sb.toString();
  }

  /**
   * Gets the set of vertices.
   * @return the vertex set
   */
  public Set<Integer> getVertexSet() {
    return chvatal.vertexSet();
  }

  /**
   * Gets the weight of a vertex.
   * @param v the vertex
   * @return the weight of the vertex
   */
  public int getWeight(final int v) {
    return weight[v];
  }

  /**
   * Gets the maximum weight of any vertex.
   * @return the maximum weight
   */
  public int getMaximumWeight() {
    return Arrays.stream(weight).max().orElse(0);
  }

  /**
   * Removes from a collection of vertices a given vertex and all vertices
   * adjacent to that vertex in the source graph.
   * @param vertices the initial collection of vertices
   * @param vertex the vertex to remove
   * @return the reduced collection of vertices
   */
  public Collection<Integer> winnow(final Collection<Integer> vertices,
                                    final Integer vertex) {
    HashSet<Integer> result = new HashSet<>(vertices);
    result.remove(vertex);
    result.removeAll(Graphs.neighborListOf(chvatal, vertex));
    return result;
  }

  /**
   * Exports the Chvatal graph as a DOT file.
   * @param file the path for the stored file
   * @exception IOException if the file cannot be saved
   */
  public void export(final String file) throws IOException {
    DOTExporter<Integer, DefaultEdge> exporter = new DOTExporter<>();
    exporter.setVertexAttributeProvider((v) -> {
      Map<String, Attribute> map = new LinkedHashMap<>();
      map.put("label", DefaultAttribute.createAttribute(v.toString()
                                                        + " (" + weight[v]
                                                        + ")"));
      return map;
    });
    FileWriter writer = new FileWriter(file);
    exporter.exportGraph(chvatal, writer);
  }
}
