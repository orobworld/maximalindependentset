package bdd;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;
import maxindset.Problem;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DirectedWeightedMultigraph;
import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;
import org.jgrapht.nio.dot.DOTExporter;

/**
 * BDD builds and solves a binary decision diagram for the maximal independent
 * set problem.
 *
 * The BDD has one layer for each node of the original graph, in node order,
 * plus a layer for the terminal node. The state of a node is the set of
 * vertices available for addition to the independent set. Arcs either add or
 * skip the vertex corresponding to the source layer. Arc weights are the weight
 * of the vertex being added or zero when a vertex is skipped.
 *
 * Because the BDD is a maximization problem (fortunately with a fixed number
 * of arcs in any root-terminus path) and JGraphT only finds shortest paths,
 * we set the weight of an arc to the difference between the maximum weight of
 * any vertex in the original graph and the weight of the vertex being added
 * along that arc (or 0 if no vertex is being added).
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class BDD {
  private final Problem problem;             // the underlying problem
  private final Graph<BDDNode, BDDArc> bdd;  // the BDD
  private final BDDNode root;                // the BDD root
  private final BDDNode terminus;            // the BDD terminus
  private final int maxWeight;               // maximum weight of any vertex
  private final GraphPath<BDDNode, BDDArc> longestPath;  // maximum weight path

  /**
   * Constructor.
   * @param prob the problem to solve
   */
  public BDD(final Problem prob) {
    // Get the problem.
    problem = prob;
    maxWeight = problem.getMaximumWeight();
    // Create an empty directed multigraph for the BDD.
    bdd = new DirectedWeightedMultigraph<BDDNode, BDDArc>(BDDArc.class);
    // Create the terminus node.
    terminus = new BDDNode(null, 0, new HashSet<>());
    bdd.addVertex(terminus);
    // Create a queue of vertices from the original graph.
    LinkedList<Integer> vertices = new LinkedList<>(problem.getVertexSet());
    // Create a set of available vertices.
    HashSet<Integer> available = new HashSet<>(vertices);
    // Build the BDD top-down. At any given point, two layers (parents and
    // children) will be in use
    // Initially, the parent layer contains just the root node.
    Integer parentVertex = vertices.poll();
    BDDLayer parents = new BDDLayer(this, parentVertex);
    root = parents.createNode(available);
    available.remove(parentVertex);
    Integer childVertex = vertices.poll();
    BDDLayer children = new BDDLayer(this, childVertex);
    // Repeat until no layers are left.
    while (childVertex != null) {
      for (BDDNode p : parents.getNodes()) {
        Collection<Integer> parentState = p.getState();
        // The zero arc (skipping the parent vertex) is always available. Add
        // a node with the same state (minus the parent vertex) to the children
        // layer and an arc to that child.
        BDDNode c = children.createNode(drop(parentState, parentVertex));
        addEdge(p, c, false);
        // The one arc (adding the parent vertex) is only available if the
        // parent vertex is in the current state.
        if (parentState.contains(parentVertex)) {
          // The child state requires dropping not just the parent vertex
          // (which is being added) but also any surviving vertices connected
          // to the parent vertex in the original graph.
          c = children.createNode(problem.winnow(parentState, parentVertex));
          addEdge(p, c, true);
        }
      }
      // The children now become the parents.
      parentVertex = childVertex;
      childVertex = vertices.poll();
      parents = children;
      children = new BDDLayer(this, childVertex);
    }
    // When there are no children left, connect the last parent layer to the
    // terminus.
    for (BDDNode p : parents.getNodes()) {
      // The false edge is always present.
      addEdge(p, terminus, false);
      // The true edge is present if there is anything left to add.
      if (!p.getState().isEmpty()) {
        addEdge(p, terminus, true);
      }
    }
    // Compute the "shortest" path from root to terminus (which is the maximum
    // weight path in the actual BDD).
    DijkstraShortestPath<BDDNode, BDDArc> dsp = new DijkstraShortestPath<>(bdd);
    longestPath = dsp.getPath(root, terminus);
  }

  /**
   * Adds a node to the BDD.
   * @param node the node to add
   */
  protected void addNode(final BDDNode node) {
    bdd.addVertex(node);
  }

  /**
   * Adds an arc to the decision diagram.
   * @param from the tail node
   * @param to the head node
   * @param add if true, the arc represents adding a vertex to the independent
   * set; if false, the vertex is being skipped
   */
  private void addEdge(final BDDNode from, final BDDNode to,
                       final boolean add) {
    Optional<Integer> v;
    double w;
    if (add) {
      v = Optional.of(from.getVertex());
      w = maxWeight - from.getWeight();
    } else {
      v = Optional.empty();
      w = maxWeight;
    }
    BDDArc e = new BDDArc(v);
    bdd.addEdge(from, to, e);
    bdd.setEdgeWeight(e, w);
  }

  /**
   * Drops a vertex from a state (if present).
   * @param state the source state
   * @param vertex the vertex to drop
   * @return the modified state
   */
  private Collection<Integer> drop(final Collection<Integer> state,
                                   final Integer vertex) {
    HashSet<Integer> newState = new HashSet<>(state);
    newState.remove(vertex);
    return newState;
  }

  /**
   * Gets the combined weight of the longest path.
   * @return the path weight
   */
  public double getLongestPathLength() {
    return maxWeight * longestPath.getLength() - longestPath.getWeight();
  }

  /**
   * Gets the maximal independent set as a string.
   * @return the MIS in string form
   */
  public String getMIS() {
    StringBuilder sb = new StringBuilder();
    // Get the labels of the edges along the longest path.
    ArrayList<Integer> vlist = new ArrayList<>();
    for (BDDArc e : longestPath.getEdgeList()) {
      Optional<Integer> v = e.getVertexAdded();
      if (v.isPresent()) {
        vlist.add(v.get());
      }
    }
    Collections.sort(vlist);
    for (Integer v : vlist) {
      sb.append("\t").append(String.format("%2d", v)).append("\n");
    }
    return sb.toString();
  }

  /**
   * Gets the weight of a vertex in the original graph.
   * @param vertex the vertex
   * @return the weight of the vertex
   */
  public double getWeight(final Integer vertex) {
    return problem.getWeight(vertex);
  }

  /**
   * Exports the BDD as a DOT file.
   * @param file the path for the stored file
   * @exception IOException if the file cannot be saved
   */
  public void export(final String file) throws IOException {
    DOTExporter<BDDNode, BDDArc> exporter = new DOTExporter<>();
    exporter.setVertexAttributeProvider((v) -> {
      Map<String, Attribute> map = new LinkedHashMap<>();
      map.put("label", DefaultAttribute.createAttribute(v.getState()
                                                         .toString()));
      return map;
    });
    exporter.setEdgeAttributeProvider((e) -> {
      Map<String, Attribute> map = new LinkedHashMap<>();
      map.put("style", DefaultAttribute.createAttribute(e.getStyle()));
      map.put("label", DefaultAttribute.createAttribute(e.getLabel()));
      return map;
    });
    FileWriter writer = new FileWriter(file);
    exporter.exportGraph(bdd, writer);
  }
}
