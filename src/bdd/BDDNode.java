package bdd;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/**
 * BDDNode holds one node in a binary decision diagram.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
final class BDDNode {
  private final HashSet<Integer> available;  // node state (available vertices)
  private final Integer vertex;              // the vertex to add or skip
  private final double weight;               // the value of the vertex

  /**
   * Constructor.
   * @param v the vertex being considered in this layer
   * @param w the value of the vertex
   * @param a the set of available vertices
   */
  BDDNode(final Integer v, final double w, final Collection<Integer> a) {
    vertex = v;
    weight = w;
    available = new HashSet<Integer>(a);
  }

  /**
   * Gets the state of the node.
   * @return the node's state
   */
  public Collection<Integer> getState() {
    return Collections.unmodifiableCollection(available);
  }

  /**
   * Gets the vertex at this node.
   * @return the vertex
   */
  public Integer getVertex() {
    return vertex;
  }

  /**
   * Gets the weight of the vertex.
   * @return the vertex weight
   */
  public double getWeight() {
    return weight;
  }

  /**
   * Expresses the node as a string.
   * @return a string listing the node
   */
  public String toString() {
    StringBuilder sb = new StringBuilder();
    if (vertex == null) {
      sb.append("terminus");
    } else {
      sb.append(vertex).append(" ( ")
        .append(String.format("%.4f", weight)).append(") [")
        .append(available).append("]");
    }
    return sb.toString();
  }
}
