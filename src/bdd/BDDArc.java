package bdd;

import java.util.Optional;
import org.jgrapht.graph.DefaultWeightedEdge;

/**
 * BDDEdge represents one arc in a binary decision diagram. In addition to a
 * weight, an arc carries a label equal to the vertex being added (if any).
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
final class BDDArc extends DefaultWeightedEdge {
  private final Optional<Integer> vertex;  // the vertex being added

  /**
   * Constructor.
   * @param v the vertex being added (empty if none)
   */
  BDDArc(final Optional<Integer> v) {
    vertex = v;
  }

  /**
   * Gets the vertex (if any) added along this arc.
   * @return the added vertex (if present)
   */
  public Optional<Integer> getVertexAdded() {
    return vertex;
  }

  /**
   * Gets the vertex being added as a string (or an empty string if the vertex
   * is not being added).
   * This is used for creating a DOT graph of the BDD.
   * @return a string showing which vertex if any is being added
   */
  public String getLabel() {
    return (vertex.isPresent()) ? vertex.get().toString() : "";
  }

  /**
   * Gets the style to use when rendering the arc in a DOT graph.
   * @return the DOT arc style
   */
  public String getStyle() {
    return (vertex.isPresent()) ? "solid" : "dotted";
  }
}
