package bdd;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

/**
 * BDDLayer holds one layer of nodes in a binary decision diagram.
 * It's main function is to avoid introduction of multiple nodes with identical
 * states within one layer.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
final class BDDLayer {
  private final BDD bdd;            // the parent bdd
  private final Integer vertex;     // the vertex being considered at this layer
  private final double weight;      // the weight of the vertex
  private final HashMap<Collection<Integer>, BDDNode> map; // maps state to node

  /**
   * Constructor.
   * @param diagram the parent BDD
   * @param v the vertex of the original graph being considered for inclusion
   * (will be null in the last layer before the terminus)
   */
  BDDLayer(final BDD diagram, final Integer v) {
    bdd = diagram;
    vertex = v;
    if (vertex == null) {
      weight = 0;
    } else {
      weight = bdd.getWeight(vertex);
    }
    map = new HashMap<>();
  }

  /**
   * Creates a new node, adds it to the BDD, and adds it to the layer.
   *
   * If a node with the same state already exists, that node is returned
   * rather than creating a new node.
   *
   * @param state the state of the node (set of available vertices
   * @return a node with the proper state
   */
  public BDDNode createNode(final Collection<Integer> state) {
    if (map.containsKey(state)) {
      // A node with the desired state already exists.
      return map.get(state);
    } else {
      // Create a new node.
      BDDNode n = new BDDNode(vertex, weight, state);
      // Add the node to the diagram.
      bdd.addNode(n);
      // Add the node to the layer.
      map.put(state, n);
      // Return the new node.
      return n;
    }
  }

  /**
   * Checks if the layer is empty.
   * @return true if empty
   */
  public boolean isEmpty() {
    return map.isEmpty();
  }

  /**
   * Gets the collection of nodes in the layer.
   * @return the node collection
   */
  public Collection<BDDNode> getNodes() {
    return Collections.unmodifiableCollection(map.values());
  }
}
