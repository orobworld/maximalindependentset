# Maximal Independent Set Problem #

### What is this repository for? ###

This project demonstrates the use of the [JGraphT](https://jgrapht.org/) library to build and solve a binary decision diagram for the maximal independent set problem (MISP). The test graph for the MISP is the Chvátal graph (12 nodes, 24 edges) with randomly generated integer weights for the vertices.

Details of the problem, and why I bothered to code this in the first place, can be found in my blog post [A BDD with JGraphT](https://orinanobworld.blogspot.com/2021/04/a-bdd-with-jgrapht.html).

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 3.0 Unported License](http://creativecommons.org/licenses/by/3.0/deed.en_US). 

